/*
 * spi.c
 *
 *  Created on: 17 cze 2015
 *      Author: Maciek
 */


#include "spi.h"

void SPI_masterInit(void) {
	/*
	 * Set power on
	 */
	LPC_SC->PCONP |= 1 << 8;

	/*
	 * Set clock
	 */
	LPC_SC->PCLKSEL0 |= 1 << 16;

	/*
	 * Select PINSEL function
	 */
	LPC_PINCON->PINSEL0 |= (3 << 30);
	LPC_PINCON->PINSEL1 |= (3 << 2) | (3 << 4);

	/*
	 * Set Control Register to desired values
	 */
	LPC_SPI->SPCR = 0x20;

	/*
	 * Set clock divider to 8, which effects in 12.5 MHz SCK frequency,
	 * as the PCLK is set to CCLK
	 */
	LPC_SPI->SPCCR = 0x08;
}

void SPI_masterTransmit (uint8_t data) {
	/* Start transmission */
	LPC_SPI->SPDR = data;
	while(!(LPC_SPI->SPSR & (1<<7)));
	uint8_t dummy = LPC_SPI->SPDR;
}

uint8_t SPI_masterReceive () {
	LPC_SPI->SPDR = 0x00;
	while(!(LPC_SPI->SPSR & (1<<7)));
	return LPC_SPI->SPDR;
}

uint8_t SPI_masterFullDuplex (uint8_t data) {
	LPC_SPI->SPDR = data;
	while(!(LPC_SPI->SPSR & (1<<7)));
	return LPC_SPI->SPDR;
}
