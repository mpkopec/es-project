/*
 * spi.h
 *
 *  Created on: 17 cze 2015
 *      Author: Maciek
 */

#ifndef SPI_H_
#define SPI_H_

#include "LPC17xx.h"

void SPI_masterInit(void);
void SPI_masterTransmit (uint8_t data);
uint8_t SPI_masterReceive ();
uint8_t SPI_masterFullDuplex (uint8_t data);

#endif /* SPI_H_ */
