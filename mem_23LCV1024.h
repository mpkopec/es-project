/*
 * mem_23LCV1024.h
 *
 *  Created on: 14 gru 2014
 *      Author: maciek
 */

#ifndef MEM_23LCV1024_H_
#define MEM_23LCV1024_H_

#include "LPC17xx.h"
#include "spi.h"

#define MEM_CS_DDR (LPC_GPIO0->FIODIR)
#define MEM_CS_PORT (LPC_GPIO0->FIOPIN)
#define MEM_CS_PIN 16
#define MEM_SELECT (LPC_GPIO0->FIOCLR = 1 << MEM_CS_PIN)
#define MEM_DESELECT (LPC_GPIO0->FIOSET = 1 << MEM_CS_PIN)

#define MEM_CMD_READ 0x03
#define MEM_CMD_WRITE 0x02
#define MEM_CMD_EDIO 0x3B
#define MEM_CMD_RSTIO 0xFF
#define MEM_CMD_RDMR 0x05
#define MEM_CMD_WRMR 0x01

extern volatile uint32_t mem_23LCV1024_addrCtr;
extern volatile uint8_t mem_23LCV1024_rwFlag;

void mem_23LCV1024_init();
uint8_t mem_23LCV1024_readStatusReg();
void mem_23LCV1024_writeStatusReg(uint8_t data);
uint32_t mem_23LCV1024_getCurrentAddress();
void mem_23LCV1024_moveToAddress();
uint8_t mem_23LCV1024_getRwStatus();
void mem_23LCV1024_setRwStatus(uint8_t status);
void mem_23LCV1024_writeByte (uint8_t data);
uint8_t mem_23LCV1024_readByte ();
void mem_23LCV1024_writeByte_woi (uint8_t data);
uint8_t mem_23LCV1024_readByte_woi ();

#endif /* MEM_23LCV1024_H_ */
