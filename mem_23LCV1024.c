/*
 * mem_23LCV1024.c
 *
 *  Created on: 14 gru 2014
 *      Author: maciek
 */

#include "mem_23LCV1024.h"
#include "timers.h"

volatile uint32_t mem_23LCV1024_addrCtr = 0;
volatile uint8_t mem_23LCV1024_rwFlag = 1;

void mem_23LCV1024_init () {
	MEM_CS_DDR |= (1<<MEM_CS_PIN);
	MEM_DESELECT;
	SPI_masterInit();
}

void mem_23LCV1024_writeStatusReg (uint8_t data) {
	MEM_DESELECT;
	TIMERS_delay_10us(1);
	MEM_SELECT;

	SPI_masterTransmit(MEM_CMD_WRMR);
	SPI_masterTransmit(data);

	MEM_DESELECT;
}

uint8_t mem_23LCV1024_readStatusReg () {
	MEM_DESELECT;
	TIMERS_delay_10us(1);
	MEM_SELECT;

	SPI_masterTransmit(MEM_CMD_RDMR);
	uint8_t data = SPI_masterReceive();

	MEM_DESELECT;

	return data;
}

uint32_t mem_23LCV1024_getCurrentAddress () {
	return mem_23LCV1024_addrCtr;
}

void mem_23LCV1024_moveToAddress (uint32_t addr) {
	mem_23LCV1024_addrCtr = addr;
	uint8_t a1 = mem_23LCV1024_addrCtr >> 16;
	uint8_t a2 = mem_23LCV1024_addrCtr >> 8;
	uint8_t a3 = mem_23LCV1024_addrCtr;

	MEM_DESELECT;
	TIMERS_delay_10us(1);
	MEM_SELECT;

	SPI_masterTransmit(mem_23LCV1024_rwFlag ? MEM_CMD_READ : MEM_CMD_WRITE);
	SPI_masterTransmit(a1);
	SPI_masterTransmit(a2);
	SPI_masterTransmit(a3);
}

uint8_t mem_23LCV1024_getRwStatus () {
	return mem_23LCV1024_rwFlag;
}

void mem_23LCV1024_setRwStatus (uint8_t status) {
	mem_23LCV1024_rwFlag = status;
}

void mem_23LCV1024_writeByte (uint8_t data) {
	SPI_masterTransmit(data);
	mem_23LCV1024_addrCtr++;
}

uint8_t mem_23LCV1024_readByte () {
	mem_23LCV1024_addrCtr++;
	return SPI_masterReceive();
}

void mem_23LCV1024_writeByte_woi (uint8_t data) {
	SPI_masterTransmit(data);
}

uint8_t mem_23LCV1024_readByte_woi () {
	return SPI_masterReceive();
}
