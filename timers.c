/*
 * timers.c
 *
 *  Created on: 16 cze 2015
 *      Author: Maciek
 */
#include "timers.h"

#include "system_LPC17xx.h"

static volatile uint32_t usCtr = 0;

void TIMERS_initTimer(LPC_TIM_TypeDef *timer) {
	/*
	 * Make sure, that the power is enabled to the timer
	 */
	if (timer == LPC_TIM0) {
		LPC_SC->PCON |= 1 << 1;
	} else if (timer == LPC_TIM1) {
		LPC_SC->PCON |= 1 << 2;
	} else if (timer == LPC_TIM0) {
		LPC_SC->PCON |= 1 << 22;
	} else {
		LPC_SC->PCON |= 1 << 23;
	}

	/*
	 * Set the timer to timer mode (not counter mode)
	 */
	timer->CCR &= ~(3 << 0);

	/*
	 * Set prescaler to 0, so every edge triggers upcounting
	 * timer counter register
	 */
	timer->PR = 0;

	/*
	 * Set up interrupt on timer's match (MR0) with simultaneous
	 * resetting the timer
	 */
	timer->MCR |= 3 << 0;

	/*
	 * Reset the timer before first use and start the timer
	 */
	timer->TCR |= 1 << 1;
	timer->TCR |= 1 << 0;
	timer->TCR &= ~(1 << 1);
}

void TIMERS_initUsTimer() {
	usCtr = 0;

	/*
	 * Additionally set up match register so interrupts
	 * will be 1 us delayed from each other
	 */
	if (SystemCoreClock > 10000000) {
		// Set up the timer properly according to SystemCoreClock
		// variable. 4 000 000 is because the peripheral clock is
		// defaultly divided by 4
		LPC_TIM0->MR0 = SystemCoreClock/400000 - 1;
	} else {
		// Set up some non-zero value, so the uC does not
		// crash when executing too large ISR procedure
		LPC_TIM0->MR0 = 30;
	}

	TIMERS_initTimer(LPC_TIM0);
}

void TIMERS_setMaxTicks(LPC_TIM_TypeDef *timer, uint32_t ticks) {
	timer->MR0 = ticks - 1;
}

void TIMERS_enableMR0Ints(LPC_TIM_TypeDef *timer) {
	timer->MCR |= 3 << 0;
}

void TIMERS_disableMR0Ints(LPC_TIM_TypeDef *timer) {
	timer->MCR &= ~(3 << 0);
}

void TIMERS_resetTimer(LPC_TIM_TypeDef *timer) {
	timer->TCR |= 1 << 1;
	timer->TCR |= 1 << 0;
}

void TIMERS_delay_10us(uint32_t us) {
	usCtr = 0;
	while (usCtr < us);
}

void TIMER0_IRQHandler() {
	if (LPC_TIM0->IR & (1 << 0))  {  /* Check if interrupt for match channel 0 occured */
		LPC_TIM0->IR |= (1 << 0);    /* Acknowledge interrupt for match channel 0 occured */
	}

	usCtr++;
}
