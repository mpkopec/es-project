/*
 * timers.h
 *
 *  Created on: 16 cze 2015
 *      Author: Maciek
 */

#ifndef TIMERS_H_
#define TIMERS_H_

#include "LPC17xx.h"

void TIMERS_initTimer(LPC_TIM_TypeDef *timer);
void TIMERS_initUsTimer();
void TIMERS_setMaxTicks(LPC_TIM_TypeDef *timer, uint32_t ticks);
void TIMERS_enableMRInts(LPC_TIM_TypeDef *timer);
void TIMERS_disableMRInts(LPC_TIM_TypeDef *timer);
void TIMERS_resetTimer(LPC_TIM_TypeDef *timer);
void TIMERS_delay_10us(uint32_t us);

#endif /* TIMERS_H_ */
