/*
    FreeRTOS V7.2.0 - Copyright (C) 2012 Real Time Engineers Ltd.


    ***************************************************************************
     *                                                                       *
     *    FreeRTOS tutorial books are available in pdf and paperback.        *
     *    Complete, revised, and edited pdf reference manuals are also       *
     *    available.                                                         *
     *                                                                       *
     *    Purchasing FreeRTOS documentation will not only help you, by       *
     *    ensuring you get running as quickly as possible and with an        *
     *    in-depth knowledge of how to use FreeRTOS, it will also help       *
     *    the FreeRTOS project to continue with its mission of providing     *
     *    professional grade, cross platform, de facto standard solutions    *
     *    for microcontrollers - completely free of charge!                  *
     *                                                                       *
     *    >>> See http://www.FreeRTOS.org/Documentation for details. <<<     *
     *                                                                       *
     *    Thank you for using FreeRTOS, and thank you for your support!      *
     *                                                                       *
    ***************************************************************************


    This file is part of the FreeRTOS distribution.

    FreeRTOS is free software; you can redistribute it and/or modify it under
    the terms of the GNU General Public License (version 2) as published by the
    Free Software Foundation AND MODIFIED BY the FreeRTOS exception.
    >>>NOTE<<< The modification to the GPL is included to allow you to
    distribute a combined work that includes FreeRTOS without being obliged to
    provide the source code for proprietary components outside of the FreeRTOS
    kernel.  FreeRTOS is distributed in the hope that it will be useful, but
    WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
    or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
    more details. You should have received a copy of the GNU General Public
    License and the FreeRTOS license exception along with FreeRTOS; if not it
    can be viewed here: http://www.freertos.org/a00114.html and also obtained
    by writing to Richard Barry, contact details for whom are available on the
    FreeRTOS WEB site.

    1 tab == 4 spaces!

    ***************************************************************************
     *                                                                       *
     *    Having a problem?  Start by reading the FAQ "My application does   *
     *    not run, what could be wrong?                                      *
     *                                                                       *
     *    http://www.FreeRTOS.org/FAQHelp.html                               *
     *                                                                       *
    ***************************************************************************


    http://www.FreeRTOS.org - Documentation, training, latest information,
    license and contact details.

    http://www.FreeRTOS.org/plus - A selection of FreeRTOS ecosystem products,
    including FreeRTOS+Trace - an indispensable productivity tool.

    Real Time Engineers ltd license FreeRTOS to High Integrity Systems, who sell
    the code with commercial support, indemnification, and middleware, under
    the OpenRTOS brand: http://www.OpenRTOS.com.  High Integrity Systems also
    provide a safety engineered and independently SIL3 certified version under
    the SafeRTOS brand: http://www.SafeRTOS.com.
*/

/*
 * This is a very simple demo that demonstrates task and queue usages only.
 * Details of other FreeRTOS features (API functions, tracing features,
 * diagnostic hook functions, memory management, etc.) can be found on the
 * FreeRTOS web site (http://www.FreeRTOS.org) and in the FreeRTOS book.
 * Details of this demo (what it does, how it should behave, etc.) can be found
 * in the accompanying PDF application note.
 *
*/

/* Kernel includes. */
#include "FreeRTOS.h"
#include "task.h"
#include "queue.h"
#include "semphr.h"
#include "timers.h"
#include "spi.h"
#include "mem_23LCV1024.h"
#include "debug_frmwrk.h"

/* Priorities at which the tasks are created. */
#define mainQUEUE_RECEIVE_TASK_PRIORITY		( tskIDLE_PRIORITY + 2 )
#define	mainQUEUE_SEND_TASK_PRIORITY		( tskIDLE_PRIORITY + 1 )

static volatile uint8_t nBytes = 0;

void UARTCmdDispatcher () {
	static volatile char cmd[5];
	for (;;) {
		_DBG("DISPATCHER\r\n");
		UART_Receive(LPC_UART1, cmd, 4, BLOCKING);
		cmd[4] = 0;

		if (!strcmp(cmd, "save")) {
			_DBG("SEND NUMBER OF BYTES TO BE SAVED:\n\r");
			UART_Receive(LPC_UART1, &nBytes, 1, BLOCKING);
			_DBG("WAITING FOR DATA TO BE SAVED\n\r");
			mem_23LCV1024_setRwStatus(0);
			mem_23LCV1024_moveToAddress(0);

			uint16_t i;
			for (i = 0; i < nBytes; i++) {
				uint8_t byte;
				UART_Receive(LPC_UART1, &byte, 1, BLOCKING);
				mem_23LCV1024_writeByte(byte);
			}

			_DBG("MEMORY WRITTEN\n\r");
		} else {
			_DBG("DATA WILL BE SENT BACK\n\r");
			mem_23LCV1024_setRwStatus(1);
			mem_23LCV1024_moveToAddress(0);

			uint16_t i;
			for (i = 0; i < nBytes; i++) {
				uint8_t byte = mem_23LCV1024_readByte();
				UART_SendByte(LPC_UART1, byte);
			}

			_DBG("\n\rEND OF MEMORY\n\r");
		}
	}
}

void LEDToggle () {
	uint8_t leds = LPC_GPIO0->FIOPIN0;
	LPC_GPIO0->FIOSET0 = ~leds;
	LPC_GPIO0->FIOCLR0 = leds;
}

void LEDToggleTask () {
	for (;;) {
		LEDToggle();
		vTaskDelay(500/portTICK_RATE_MS);
	}
}


/*-----------------------------------------------------------*/

int main(void)
{
	/* Initialise P0_22 for the LED. */
	LPC_PINCON->PINSEL1	&= ( ~( 3 << 12 ) );
	LPC_GPIO0->FIODIR0  = 0xff;
	LPC_GPIO0->FIODIR |= 1 << 16;

	debug_frmwrk_init();

	TIMERS_initUsTimer();
	NVIC_SetPriority (TIMER0_IRQn, 1);            // Set Timer priority
	NVIC_EnableIRQ (TIMER0_IRQn);
	mem_23LCV1024_init();
	/*
	 * Create tasks
	 */
	xTaskCreate(UARTCmdDispatcher, (char *) "cmdDispatcher", configMINIMAL_STACK_SIZE, NULL, tskIDLE_PRIORITY + 4, NULL);
	xTaskCreate(LEDToggleTask, (char *) "LEDs", configMINIMAL_STACK_SIZE, NULL, tskIDLE_PRIORITY + 4, NULL);

	/* Start the tasks running. */
	vTaskStartScheduler();

	/* If all is well we will never reach here as the scheduler will now be
	running.  If we do reach here then it is likely that there was insufficient
	heap available for the idle task to be created. */
	for( ;; );
}
